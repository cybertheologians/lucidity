﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Lucidity.Utility
{
	internal class GreaterThanConverter : IValueConverter, IMultiValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			// cast to comparable
			IComparable comp = value as IComparable;
			if (comp == null)
				return DependencyProperty.UnsetValue;

			return comp.CompareTo(parameter) > 0;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			// only continue if there are exactly two values
			if (values.Length != 2)
				return DependencyProperty.UnsetValue;

			// cast to comparable
			IComparable comp = values[0] as IComparable;
			if (comp == null)
				return DependencyProperty.UnsetValue;

			return comp.CompareTo(values[1]) > 0;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
