﻿<Window x:Class="Lucidity.Previewer"
		xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
		xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
		xmlns:my="clr-namespace:Lucidity"
		xmlns:util="clr-namespace:Lucidity.Utility"
		xmlns:s="clr-namespace:System;assembly=mscorlib"
		Background="{StaticResource MainBackgroundBrush}"
		AllowDrop="True"
		Drop="Window_Drop"
		Loaded="Window_Loaded">
	<Window.Resources>

		<my:ImageViewModel x:Key="ViewModel" />
		<util:GreaterThanConverter x:Key="GreaterThan" />
		<util:ManyImageWidthConverter x:Key="ManyImageWidthConverter" />

		<my:CloseCommand x:Key="CloseCommand" />
		<my:CloseAllButThisCommand x:Key="CloseAllButThisCommand" />

		<Style x:Key="ListBoxItemStyle"
			   TargetType="{x:Type ListBoxItem}">
			<Setter Property="FocusVisualStyle"
					Value="{x:Null}" />
			<Setter Property="OverridesDefaultStyle"
					Value="True" />
			<Setter Property="Margin"
					Value="0" />
			<Setter Property="Padding"
					Value="0" />
			<Setter Property="HorizontalContentAlignment"
					Value="Center" />
			<Setter Property="VerticalContentAlignment"
					Value="Center" />
			<Setter Property="SnapsToDevicePixels"
					Value="True" />
			<Setter Property="Template">
				<Setter.Value>
					<ControlTemplate TargetType="{x:Type ListBoxItem}">
						<Border Background="Transparent">
							<Border x:Name="mainBorder"
									SnapsToDevicePixels="True"
									Margin="12"
									BorderThickness="4"
									VerticalAlignment="Center"
									HorizontalAlignment="Center">
								<ContentPresenter ContentTemplate="{TemplateBinding ContentTemplate}"
												  SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"
												  HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
												  VerticalAlignment="{TemplateBinding VerticalContentAlignment}" />
							</Border>
						</Border>

						<ControlTemplate.Triggers>
							<Trigger Property="IsMouseOver"
									 Value="True">
								<Setter TargetName="mainBorder"
										Property="BorderBrush"
										Value="{StaticResource ListBoxItemOverBrush}" />
							</Trigger>
							<Trigger Property="IsSelected"
									 Value="True">
								<Setter TargetName="mainBorder"
										Property="BorderBrush"
										Value="{StaticResource ListBoxItemSelectedBrush}" />
							</Trigger>
							<MultiTrigger>
								<MultiTrigger.Conditions>
									<Condition Property="IsMouseOver"
											   Value="True" />
									<Condition Property="IsSelected"
											   Value="True" />
								</MultiTrigger.Conditions>
								<Setter TargetName="mainBorder"
										Property="BorderBrush"
										Value="{StaticResource ListBoxItemOverBrush}" />
							</MultiTrigger>
						</ControlTemplate.Triggers>
					</ControlTemplate>
				</Setter.Value>
			</Setter>
		</Style>

		<DataTemplate x:Key="ListViewItemTemplate">
			<Image Source="{Binding Thumbnail}" />
		</DataTemplate>

		<Style x:Key="ItemTemplateWrapperStyle"
			   TargetType="FrameworkElement">
			<Setter Property="Margin"
					Value="8" />
		</Style>

		<DataTemplate x:Key="ImageTemplate">
			<Grid x:Name="Wrapper"
				  HorizontalAlignment="Center"
				  VerticalAlignment="Center"
				  Background="Transparent"
				  PreviewMouseDown="Wrapper_PreviewMouseDown">
				<Image Margin="8"
					   ToolTip="{Binding FileName}"
					   VerticalAlignment="Center"
					   HorizontalAlignment="Center"
					   Source="{Binding Source}">
					<Image.ContextMenu>
						<ContextMenu>
							<MenuItem DataContext="{StaticResource CloseCommand}"
									  Command="{Binding}"
									  Header="{Binding Path=Text}"
                                      CommandTarget="{Binding RelativeSource={RelativeSource TemplatedParent}}"
									  CommandParameter="{Binding RelativeSource={RelativeSource TemplatedParent}, Path=Content}" />
							<MenuItem DataContext="{StaticResource CloseAllButThisCommand}"
									  Command="{Binding}"
									  Header="{Binding Path=Text}"
                                      CommandTarget="{Binding RelativeSource={RelativeSource TemplatedParent}}"
									  CommandParameter="{Binding RelativeSource={RelativeSource TemplatedParent}, Path=Content}" />
						</ContextMenu>
					</Image.ContextMenu>
				</Image>
				<Button x:Name="CloseButton"
						Style="{StaticResource CloseXButtonStyle}"
						Opacity="0"
						HorizontalAlignment="Right"
						VerticalAlignment="Top"
                        DataContext="{StaticResource CloseCommand}"
						Command="{Binding}"
                        CommandParameter="{Binding RelativeSource={RelativeSource TemplatedParent}, Path=Content}" />
			</Grid>

			<DataTemplate.Triggers>
				<Trigger SourceName="Wrapper"
						 Property="IsMouseOver"
						 Value="True">
					<Trigger.EnterActions>
						<BeginStoryboard>
							<Storyboard>
								<DoubleAnimation Storyboard.TargetName="CloseButton"
												 Storyboard.TargetProperty="Opacity"
												 To="1"
												 Duration="0:0:0.2" />
							</Storyboard>
						</BeginStoryboard>
					</Trigger.EnterActions>
					<Trigger.ExitActions>
						<BeginStoryboard>
							<Storyboard>
								<DoubleAnimation Storyboard.TargetName="CloseButton"
												 Storyboard.TargetProperty="Opacity"
												 Duration="0:0:0.5" />
							</Storyboard>
						</BeginStoryboard>
					</Trigger.ExitActions>
				</Trigger>
			</DataTemplate.Triggers>
		</DataTemplate>

		<DataTemplate x:Key="SingleItemTemplate">
			<ContentControl Style="{StaticResource ItemTemplateWrapperStyle}"
							ContentTemplate="{StaticResource ImageTemplate}"
							Content="{Binding SelectedItems[0]}"
							DataContext="{Binding ElementName=ImageListBox}" />
		</DataTemplate>

		<DataTemplate x:Key="DoubleItemTemplate">
			<Grid x:Name="OuterGrid"
				  Style="{StaticResource ItemTemplateWrapperStyle}">
				<Grid.ColumnDefinitions>
					<ColumnDefinition Width="*" />
					<ColumnDefinition Width="*" />
				</Grid.ColumnDefinitions>
				<Grid.RowDefinitions>
					<RowDefinition Height="*" />
					<RowDefinition Height="*" />
				</Grid.RowDefinitions>

				<ContentControl x:Name="Image1"
								Grid.ColumnSpan="1"
								Grid.RowSpan="2"
								ContentTemplate="{StaticResource ImageTemplate}"
								Content="{Binding SelectedItems[0]}"
								DataContext="{Binding ElementName=ImageListBox}" />
				<ContentControl x:Name="Image2"
								Grid.Column="1"
								Grid.Row="0"
								Grid.ColumnSpan="1"
								Grid.RowSpan="2"
								ContentTemplate="{StaticResource ImageTemplate}"
								Content="{Binding SelectedItems[1]}"
								DataContext="{Binding ElementName=ImageListBox}" />
			</Grid>

			<DataTemplate.Triggers>
				<DataTrigger Value="True">
					<DataTrigger.Binding>
						<MultiBinding Converter="{StaticResource GreaterThan}">
							<Binding Path="ActualHeight"
									 ElementName="OuterGrid" />
							<Binding Path="ActualWidth"
									 ElementName="OuterGrid" />
						</MultiBinding>
					</DataTrigger.Binding>
					<Setter TargetName="Image2"
							Property="Grid.Row"
							Value="1" />
					<Setter TargetName="Image2"
							Property="Grid.Column"
							Value="0" />
					<Setter TargetName="Image2"
							Property="Grid.RowSpan"
							Value="1" />
					<Setter TargetName="Image2"
							Property="Grid.ColumnSpan"
							Value="2" />
					<Setter TargetName="Image1"
							Property="Grid.RowSpan"
							Value="1" />
					<Setter TargetName="Image1"
							Property="Grid.ColumnSpan"
							Value="2" />
				</DataTrigger>
			</DataTemplate.Triggers>
		</DataTemplate>

		<DataTemplate x:Key="MoreItemTemplate">
			<Border x:Name="ImageListBoxWrapper"
					Style="{StaticResource ItemTemplateWrapperStyle}">
				<ItemsControl DataContext="{Binding ElementName=ImageListBox}"
							  ItemTemplate="{StaticResource ImageTemplate}"
							  ItemsSource="{Binding SelectedItems}">
					<ItemsControl.ItemsPanel>
						<ItemsPanelTemplate>
							<WrapPanel IsItemsHost="True"
									   Orientation="Horizontal">
								<WrapPanel.ItemWidth>
									<MultiBinding Converter="{StaticResource ManyImageWidthConverter}">
										<MultiBinding.Bindings>
											<Binding Path="ActualWidth"
													 ElementName="ImageListBoxWrapper" />
											<Binding Path="ActualHeight"
													 ElementName="ImageListBoxWrapper" />
											<Binding Path="DataContext.SelectedItems.Count"
													 RelativeSource="{RelativeSource AncestorType={x:Type ItemsControl}}" />
										</MultiBinding.Bindings>
										<MultiBinding.ConverterParameter>
											<s:String>Width</s:String>
										</MultiBinding.ConverterParameter>
									</MultiBinding>
								</WrapPanel.ItemWidth>
								<WrapPanel.ItemHeight>
									<MultiBinding Converter="{StaticResource ManyImageWidthConverter}">
										<MultiBinding.Bindings>
											<Binding Path="ActualWidth"
													 ElementName="ImageListBoxWrapper" />
											<Binding Path="ActualHeight"
													 ElementName="ImageListBoxWrapper" />
											<Binding Path="DataContext.SelectedItems.Count"
													 RelativeSource="{RelativeSource AncestorType={x:Type ItemsControl}}" />
										</MultiBinding.Bindings>
										<MultiBinding.ConverterParameter>
											<s:String>Height</s:String>
										</MultiBinding.ConverterParameter>
									</MultiBinding>
								</WrapPanel.ItemHeight>
							</WrapPanel>
						</ItemsPanelTemplate>
					</ItemsControl.ItemsPanel>
				</ItemsControl>
			</Border>
		</DataTemplate>

		<DataTemplate x:Key="MainViewerTemplate">
			<Grid Background="{StaticResource MainBackgroundShadowBrush}">
				<Image x:Name="Logo"
					   Source="images/LucidityLogo.png"
					   Width="500"
					   Height="100"
					   SnapsToDevicePixels="True"
					   Visibility="Collapsed" />

				<ContentPresenter x:Name="MainViewer"
								  ContentTemplate="{StaticResource MoreItemTemplate}"
								  Effect="{StaticResource ImageDropShadowEffect}" />
			</Grid>

			<DataTemplate.Triggers>
				<DataTrigger Binding="{Binding SelectedItems.Count, ElementName=ImageListBox}"
							 Value="0">
					<Setter TargetName="Logo"
							Property="Visibility"
							Value="Visible" />
				</DataTrigger>
				<DataTrigger Binding="{Binding SelectedItems.Count, ElementName=ImageListBox}"
							 Value="1">
					<Setter TargetName="MainViewer"
							Property="ContentTemplate"
							Value="{StaticResource SingleItemTemplate}" />
				</DataTrigger>
				<DataTrigger Binding="{Binding SelectedItems.Count, ElementName=ImageListBox}"
							 Value="2">
					<Setter TargetName="MainViewer"
							Property="ContentTemplate"
							Value="{StaticResource DoubleItemTemplate}" />
				</DataTrigger>
			</DataTemplate.Triggers>
		</DataTemplate>
	</Window.Resources>

	<DockPanel Background="{StaticResource MainBackgroundTextureBrush}">

		<ListBox x:Name="ImageListBox"
				 DataContext="{StaticResource ViewModel}"
				 DockPanel.Dock="Bottom"
				 Background="{StaticResource ListBoxBackgroundBrush}"
				 BorderThickness="0"
				 ItemTemplate="{StaticResource ListViewItemTemplate}"
				 ItemsSource="{Binding Path=Images}"
				 ScrollViewer.VerticalScrollBarVisibility="Hidden"
				 ScrollViewer.CanContentScroll="False"
				 SelectionMode="Extended"
				 Effect="{StaticResource ImageDropShadowEffect}">
			<ListBox.ItemContainerStyle>
				<Style BasedOn="{StaticResource ListBoxItemStyle}"
					   TargetType="{x:Type ListBoxItem}">
					<Setter Property="Width"
							Value="120" />
					<Setter Property="Height"
							Value="120" />
				</Style>
			</ListBox.ItemContainerStyle>
			<ListBox.ItemsPanel>
				<ItemsPanelTemplate>
					<StackPanel IsItemsHost="True"
								Orientation="Horizontal"
								HorizontalAlignment="Center" />
				</ItemsPanelTemplate>
			</ListBox.ItemsPanel>
		</ListBox>

		<ContentPresenter DataContext="{Binding ElementName=ImageListBox}"
						  ContentTemplate="{StaticResource MainViewerTemplate}" />

	</DockPanel>
</Window>
