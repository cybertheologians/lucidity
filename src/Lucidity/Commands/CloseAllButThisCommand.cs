﻿using System.Windows.Input;

namespace Lucidity
{
	public sealed class CloseAllButThisCommand : ContextMenuCommand
	{
		public CloseAllButThisCommand()
			: base("Close all but this") { }

		public override void Command_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			Previewer previewer = (Previewer)sender;
			e.CanExecute =  previewer.ImageListBox.SelectedItems.Count > 1;
			e.Handled = true;
		}

		public override void Command_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			Previewer previewer = (Previewer)sender;
			ImageDataModel imageDataModel = (ImageDataModel)e.Parameter;
			previewer.SelectSingleImage(imageDataModel);
		}
	}
}
