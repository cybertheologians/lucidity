﻿using System.Windows.Input;

namespace Lucidity
{
	public sealed class CloseCommand : ContextMenuCommand
	{
		public CloseCommand()
			: base("Close") { }

		public override void Command_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = true;
			e.Handled = true;
		}

		public override void Command_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			Previewer previewer = (Previewer)sender;
			ImageDataModel imageDataModel = (ImageDataModel)e.Parameter;
			previewer.CloseImage(imageDataModel);
		}
	}
}
