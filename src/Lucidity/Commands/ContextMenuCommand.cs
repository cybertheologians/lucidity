﻿using System.Windows.Input;

namespace Lucidity
{
	public abstract class ContextMenuCommand : RoutedUICommand
	{
		protected ContextMenuCommand(string text)
		{
			Text = text;
			m_commandBinding = new CommandBinding(this, Command_Executed, Command_CanExecute);
		}
		
		public CommandBinding Binding
		{
			get { return m_commandBinding; }
		}
		
		public abstract void Command_CanExecute(object sender, CanExecuteRoutedEventArgs e);
		public abstract void Command_Executed(object sender, ExecutedRoutedEventArgs e);
		
		CommandBinding m_commandBinding;
	}
}
