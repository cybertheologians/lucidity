﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Cybertheologians.Utility.Images;
using NLog;

namespace Lucidity
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class Previewer : Window
	{
		public Previewer()
		{
			if (File.Exists("Lucidity.log"))
				File.WriteAllText("Lucidity.log", string.Empty);
			
			s_logger.Info("Instantiating Previewer ...");
			InitializeComponent();
			m_model = (ImageViewModel)this.Resources["ViewModel"];
			
			ContextMenuCommand closeImageCommand = (CloseCommand)this.Resources["CloseCommand"];
			ContextMenuCommand closeAllButThisCommand = (CloseAllButThisCommand)this.Resources["CloseAllButThisCommand"];

			this.CommandBindings.Add(closeImageCommand.Binding);
			this.CommandBindings.Add(closeAllButThisCommand.Binding);
		}

		internal void SelectSingleImage(ImageDataModel toSelect)
		{
			ImageListBox.UnselectAll();
			ImageListBox.SelectedItem = toSelect;
			s_logger.Info("Selecting {0} as single image.", toSelect.FileName);
		}

		internal void CloseImage(ImageDataModel toRemove)
		{
			if (toRemove == null)
				throw new InvalidOperationException("ImageDataModel does not exist in ImageListBox.SelectedItems.");

			ImageListBox.SelectedItems.Remove(toRemove);
			s_logger.Info("Removing {0} from selected items.", toRemove.FileName);
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			s_logger.Info("Finding and loading all images in current directory.");
			m_model.GetAllImagesInDirectory(Environment.CurrentDirectory);
		}

		private void Window_Drop(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop))
			{
				s_logger.Info("Getting data format of images dropped into Window.");
				string[] data = (string[])e.Data.GetData(DataFormats.FileDrop);

				s_logger.Info("Clearing Images of {0} ImageDataModels.", m_model.Images.Count());
				m_model.Images.Clear();

				m_model.LoadDataModelsFromFilenames(data);
			}
			else
			{
				s_logger.Debug("Error! This should never happen on Window drop!");
			}
		}

		private void Wrapper_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			s_logger.Info("Image wrapper PreviewMouseDown event fired.");

			if (e.ClickCount == 2)
			{
				s_logger.Info("Image wrapper double-clicked.");
				Panel wrapper = (Panel)sender;

				ImageSource source = ImageSourceUtility.ExtractSingleImageSource(wrapper);
				s_logger.Info("{0} found as image source.", source.ToString());

				ImageDataModel toSelect = GetImageDataModel(source);
				SelectSingleImage(toSelect);
			}
		}

		private ImageDataModel GetImageDataModel(ImageSource source)
		{
			var selectedItems = ImageListBox.SelectedItems.Cast<ImageDataModel>().ToArray();
			s_logger.Info("Grabbing {0} selected items.", selectedItems.Count());

			ImageDataModel imageDataModel = selectedItems.SingleOrDefault(x => ((ImageSource)x.Source) == source);
			s_logger.Info("Finding data model for {0}.", Path.GetFileName(source.ToString()));
			return imageDataModel;
		}

		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
		ImageViewModel m_model;
	}
}
