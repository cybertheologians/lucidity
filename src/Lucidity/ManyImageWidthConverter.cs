﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Lucidity.Utility
{
	internal class ManyImageWidthConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			// only continue if there are exactly three values
			if (values.Length != 3)
				throw new ArgumentException("There must be three values.", "values");
			if ((string)parameter != "Width" && (string)parameter != "Height")
				throw new ArgumentException("parameter must be 'Width' or 'Height'.", "parameter");

			int DisplayCount = (values[2] == DependencyProperty.UnsetValue) ? 0 : (int)values[2];
			if (DisplayCount > 0)
			{
				double DisplayWidth = (double)values[0];
				double DisplayHeight = (double)values[1];

				double InitialRatio = DisplayHeight / DisplayWidth;

				double Columns = Math.Max(Math.Floor(Math.Sqrt(DisplayCount / InitialRatio)), 1);
				double Rows = Math.Ceiling(DisplayCount / Columns);

				if (Columns * Rows > (Columns + 1) * (Math.Ceiling(DisplayCount / (Columns + 1))))
				{
					Columns++;
					Rows = Math.Ceiling(DisplayCount / Columns);
				}

				if ((string)parameter == "Width")
				{
					return DisplayWidth / Columns;
				}
				else
				{
					return DisplayHeight / Rows;
				}
			}
			else
			{
				return 0;
			}
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
