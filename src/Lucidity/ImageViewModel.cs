﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cybertheologians.Utility;
using NLog;

namespace Lucidity
{
	public class ImageViewModel
	{
		public ImageViewModel()
		{
			s_logger.Info("Instantiating ImageViewModel");
			Images = new ObservableCollection<ImageDataModel>();
		}

		public ObservableCollection<ImageDataModel> Images { get; set; }

		public void GetAllImagesInDirectory(string directoryName)
		{
			s_logger.Info(string.Format("Extracting image filenames from directory {0}.", directoryName));
			string[] fileNames = GetAllImageFilenamesInDirectory(directoryName);

			s_logger.Info(string.Format("Instantiating ImageDataModels from {0} filenames", fileNames.Count()));
			LoadDataModelsFromFilenames(fileNames);
		}

		public void LoadDataModelsFromFilenames(IEnumerable<string> imageFileNames)
		{
			LoadDataModelsCore(imageFileNames);
		}

		private static string[] GetAllImageFilenamesInDirectory(string directoryName)
		{
			return Directory.GetFiles(directoryName)
				.Where(x => s_fileTypes.Contains(FileUtility.GetTrimmedInvariantExtension(x)))
				.ToArray();
		}

		private void LoadDataModelsCore(IEnumerable<string> imageFileNames)
		{
			foreach (string fileName in imageFileNames)
			{
				Task<ImageDataModel> task = Task.Run(() => LoadDataModelCore(fileName));

				ImageDataModel dataModel = task.Result;
				s_logger.Info(string.Format("ImageDataModel for {0} added to images.", dataModel.FileName));
				Images.Add(dataModel);
			}
		}

		private ImageDataModel LoadDataModelCore(string fileName)
		{
			return new ImageDataModel(fileName);
		}

		static readonly string[] s_fileTypes = new string[] { "jpg", "png", "bmp", "dib", "gif", "jfif", "jpe", "jxr", "tif", "tiff", "wdp" };
		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}
