﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Media.Imaging;
using Cybertheologians.Utility.Images;
using NLog;

namespace Lucidity
{
	public class ImageDataModel : INotifyPropertyChanged
	{
		public ImageDataModel(string filepath)
		{
			m_fullFilePath = filepath;
			string fileName = Path.GetFileName(m_fullFilePath);
			s_logger.Info(string.Format("Instantiating ImageDataModel from {0}.", fileName));
			FileName = m_fullFilePath;
			s_logger.Info(string.Format("ImageDataModel instantiated from {0}.", fileName));
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void OnPropertyChanged(string property)
		{
			s_logger.Info(string.Format("OnPropertyChanged event raised by change to {0} property.", property));
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(property));
				s_logger.Info(string.Format("PropertyChangedEventHandler for {0} property fired.", property));
			}
			else
			{
				s_logger.Info(string.Format("PropertyChangedEventHandler for {0} property failed to fire.", property));
			}
		}

		public string FileName { get; set; }
		public BitmapImage Source
		{
			get
			{
				if (m_source == null)
					m_source = InitializeSource(m_fullFilePath, SourceKind.Full);
				return m_source;
			}
		}

		public BitmapImage Thumbnail
		{
			get
			{
				if (m_thumbnail == null)
					m_thumbnail = InitializeSource(m_fullFilePath, SourceKind.Thumbnail);
				return m_thumbnail;
			}
		}

		private static BitmapImage InitializeSource(string fullFilePath, SourceKind sourceKind)
		{
			BitmapImage source = new BitmapImage();
			source.BeginInit();
			source.UriSource = new Uri(fullFilePath);
			source.CacheOption = BitmapCacheOption.OnDemand;
			source.CreateOptions = BitmapCreateOptions.DelayCreation;

			if (sourceKind == SourceKind.Thumbnail)
				BitmapImageUtility.SetDecodePixelSize(source, s_optimalThumbnailPixelSize);

			source.EndInit();
			return source;
		}

		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
		static readonly int s_optimalThumbnailPixelSize = 96;
		readonly string m_fullFilePath;
		BitmapImage m_source;
		BitmapImage m_thumbnail;
	}
}
