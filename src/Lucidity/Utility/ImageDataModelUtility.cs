﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using NLog;

namespace Lucidity.Utility
{
	public static class ImageDataModelUtility
	{
		public static IEnumerable<ImageDataModel> FilterDataModelsBySource(this IEnumerable<ImageDataModel> images, IEnumerable<ImageSource> sources)
		{
			s_logger.Info(string.Format("Filtering images by {0} sources.", sources.Count()));
			foreach (ImageSource source in sources)
			{
				s_logger.Info(string.Format("Checking images for {0} ...", source.ToString()));
				var result = images.FirstOrDefault(x => x.Source == source);
				if (result != null)
				{
					s_logger.Info(string.Format("{0} found.", source.ToString()));
					yield return result;
				}
				else
				{
					s_logger.Info(string.Format("{0} not found.", source.ToString()));
				}
			}
		}

		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}
