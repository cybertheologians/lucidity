﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Lucidity.Utility
{
	internal class ScrollOffsetRatioConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			double fResult;

			// only continue if there are exactly five values
			if (values.Length != 5)
				throw new ArgumentException("There must be 5 values.", "values");

			double fCurrentOffset = (double)values[0];
			double fScrollableLength = (double)values[1];
			double fVisibleLength = (double)values[2];
			bool bUseTop = (bool)values[3];
			double fMaxResult = (double)values[4];

			if (fScrollableLength == 0 || fVisibleLength == 0)
			{
				fResult = bUseTop ? 0 : 1;
			}
			else if (bUseTop)
			{
				fResult = fCurrentOffset / fVisibleLength;
				fResult = (fResult * fVisibleLength > fMaxResult) ? fMaxResult / fVisibleLength : fResult;
			}
			else
			{
				fResult = 1.0 - (fScrollableLength - fCurrentOffset) / fVisibleLength;
				fResult = (fResult * fVisibleLength < (fVisibleLength - fMaxResult)) ? (fVisibleLength - fMaxResult) / fVisibleLength : fResult;
			}

			return fResult;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
